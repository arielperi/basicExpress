var webApp = angular.module('application', ['ui.router', 'mainCtrl', 'loginCtrl', 'authService']);


webApp.config(function($httpProvider){
  $httpProvider.interceptors.push('AuthInterceptor');
});

webApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){

  // Now set up the states
  $stateProvider
    .state('state1', {
      url: "/state1",
      // I must enter the complete path, i.e. including webApp,
      //because it's served from within the public folder as defined in node's app.js file
      templateUrl: "webApp/views/pages/main.html"
    })
    .state('login', {
      url: "/login",
      templateUrl: "webApp/views/pages/login.html",
      controller: 'loginController'
    })
    .state('signup', {
      url: "/signup",
      templateUrl: "webApp/views/pages/signup.html"
    })
    .state('contact-us', {
      url: "/contact-us",
      // I must enter the complete path, i.e. including webApp,
      //because it's served from within the public folder as defined in node's app.js file
      templateUrl: "webApp/views/pages/contact-us.html"
    });
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise("/login");
}]);
