var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;


var User = require('../models/user.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.render('users/list', { title: 'Users space', name: 'GAD', homeActive: '' });
});

router.get('/list', function(req, res, next) {
  res.render('users/list', { title: 'Members', name: 'GAD', homeActive: '' });
});

router.get('/register', function(req, res, next) {
  res.render('users/register', { title: 'Register', homeActive: '', errors: {}});
});

router.post('/register', function(req, res, next) {
  var userName = req.body.username;
  var pass = req.body.password;
  var pass2 = req.body.password2;
  var email = req.body.email;

  // Dealing with the profile image
  var uploadedImg = {};
  if(req.file) {
    uploadedImg.originalName = req.file.originalname;
    uploadedImg.originalEncoding = req.file.encoding;
    uploadedImg.originalMimeType = req.file.mimetype;
    uploadedImg.name = req.file.filename;
    uploadedImg.path = req.file.path;
    uploadedImg.size = req.file.size;
  } else {
    uploadedImg.newName = 'noImega.png';
  }

  // Form validation
  req.checkBody('username', 'Name field is required').notEmpty();
  req.checkBody('password', 'Password field is required').notEmpty();
  req.checkBody('password2', 'Confirmation password must match').equals(pass);
  req.checkBody('email', 'Email field is required').notEmpty();
  req.checkBody('email', 'You must enter a valide email').isEmail();

  var errors = req.validationErrors();
  if(errors) {
    res.render('users/register', {
      title: 'Register',
      homeActive: '',
      errors: JSON.stringify(errors),
      userName: userName,
      email: email
    });
  } else {
    var newUser = new User({
      userName: userName,
      password: pass,
      email: email,
      avatar: uploadedImg.name
    });
    console.log(newUser);
    User.createUser(newUser, function(err, user) {
      if(err) {
        throw err;
      };
      req.flash('success', 'You are now a user, congrats!');
    });
    res.redirect('/');
  }

});

router.get('/sign-in', function(req, res, next) {
  res.render('users/sign-in', { title: 'Sign in', homeActive: '' });
});


passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.getUserById(id, function(err, user) {
    done(err, user);
  });
});

passport.use('local', new LocalStrategy(
  function(userName, password, done) {
    User.getUserByUserName(userName, function(err, user) {
      if(err) throw err;
      if(!user) {
        console.log('Unknown user');
        return done(null, false, {message: 'Unknown user'});
      }
      console.log(user.password);
      User.checkPassword(password, user.password, function(err, isMatch){
        if (err) throw err;
        if(isMatch) {
            return done(null, user);
        } else {
          console.log('Invalid password');
          return done(null, false, {message: 'Invalid password'});
        }
      });
    });
  }
));


router.post('/sign-in', passport.authenticate('local', {failureRedirect: '/users/sign-in', failureFlash: 'Invalid username or password'}), function(req, res) {
  console.log('Authentication succesful');
  req.flash('success ','You are logged in');
  res.redirect('/users/list')
});

router.get('/logout', function(req, res){
  req.logout();
  req.flash('success', 'You have logged out');
  res.redirect('/users/sign-in');
});

module.exports = router;
