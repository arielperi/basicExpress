var mongoose = require('mongoose');
var db = mongoose.connection;
var bcrypt = require('bcrypt-nodejs');

// User schema
var UserSchema = mongoose.Schema({
    userName: {type: String, index: true},
    password: {type: String, required: true, bcrypt: true},
    email: {type: String},
    avatar: {type: String}
});

var User = module.exports = mongoose.model('User', UserSchema);

module.exports.createUser = function(newUser, callback) {
  bcrypt.hash(newUser.password, null, null, function(err, hash){
      if(err) throw err;
      newUser.password = hash;
  });

  console.log('babam');
  newUser.save(callback);
};

module.exports.getUserById = function(id, callback) {
  User.findById(id, callback);
};

module.exports.checkPassword = function(candidatePassword, hash, callback) {
  bcrypt.compare(candidatePassword, hash, function(err, isMatch){
    if(err){
      return callback(err);
    } else {
      callback(null, isMatch);
    }
  });
}

module.exports.getUserByUserName = function(userName, callback) {
  var query = {userName: userName};
  User.findOne(query, callback);
};
